##**Evermos QA Engineer Assessment**

This repo contains test automation source code for fulfilling the requirement for Evermos technical assessment for QA Engineer position. The script was build by using **Java Cucumber Junit Framework**.

The Gerkhin file contain of the scenario that will be execute Automaticly which is on /src/test/resources/

The implementation of the gerkhin syntax was put on java file at /src/test/java/StepDefinitions/

---

## Prerequisite
To run the project, you need to install to MacOS environment .

1. Java JDK ver. 1.8.
2. Apache Maven
3. Chrome Version 84.0.4147.105
4. chromedriver (in webdriver folder is chromedriver for MacOS)

---

## Running The Script
MacOs : You can run scrip by using maven command "mvn clean test"

Linux & Windows:

1. download chromedriver that suitable with chrome browser
2. open file evermosweb/src/test/java/StepDefinitions/Hook.java
3. change line 23 into path where chromedriver exist
4. run scrip by using maven command "mvn clean test"


---

## Author

1. Satrio Ajie Wijaya
