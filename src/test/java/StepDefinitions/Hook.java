package StepDefinitions;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Hook {

    @After
    public void closeBrowser(Scenario scenario){
        final byte[] screenshot = ((TakesScreenshot) BaseStep.driver).getScreenshotAs(OutputType.BYTES);
        scenario.embed(screenshot, "image/png"); // ... and embed it in
        BaseStep.driver.quit();
    }

    @Before
    public void initBrowser(){
        System.setProperty("webdriver.chrome.driver", "webdriver/chromedriver");
        BaseStep.driver = new ChromeDriver();
        BaseStep.driver.get("https://evermos.com/");
        BaseStep.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

}
