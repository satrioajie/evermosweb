package StepDefinitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Pembelian extends BaseStep {

    @When("^user memilih produk \"([^\"]*)\" di halaman rekomendasi$")
    public void user_memilih_produk_di_halaman_rekomendasi(String prodName) throws Throwable {
        rekomendasiPage.clickProduct(prodName);
    }

    @When("^user memilih ukuran \"([^\"]*)\" di halaman detail produk$")
    public void user_memilih_ukuran_di_halaman_detail_produk(String size) throws Throwable {
        detailProdukPage.selectUkuran(size);
    }

    @When("^user memilih warna \"([^\"]*)\" di halaman detail produk$")
    public void user_memilih_warna_di_halaman_detail_produk(String warna) throws Throwable {
        detailProdukPage.selectWarna(warna);
    }

    @When("^user mengklik Order di halaman detail produk$")
    public void user_mengklik_Order_di_halaman_detail_produk() throws Throwable {
        detailProdukPage.clickOrderButton();
    }

    @When("^user memasukkan keyword \"([^\"]*)\" di halaman alamat pengiriman$")
    public void user_memasukkan_keyword_di_halaman_alamat_pengiriman(String keyword) throws Throwable {
        alamatPengirimanPage.searchKeywordAlamat(keyword);
    }

    @When("^user memilih alamat \"([^\"]*)\" sebagai alamat tujuan pengiriman di halaman alamat pengiriman$")
    public void user_memilih_alamat_sebagai_alamat_tujuan_pengiriman_di_halaman_alamat_pengiriman(String alamat) throws Throwable {
        alamatPengirimanPage.selectAlamat(alamat);
    }

    @When("^user mengklik tombol Pilih Alamat di halaman alamat pengiriman$")
    public void user_mengklik_tombol_Pilih_Alamat_di_halaman_alamat_pengiriman() throws Throwable {
        alamatPengirimanPage.clickPilihAlamat();
    }

    @Then("^notifikasi produk berhasil ditambahkan ke keranjang muncul$")
    public void notifikasi_produk_berhasil_ditambahkan_ke_keranjang_muncul() throws Throwable {
        detailProdukPage.validateSuccessAddToCart();
    }

    @Then("^user mengklik tombol Lihat Keranjang pada notifikasi produk berhasil ditambahkan ke keranjang$")
    public void user_mengklik_tombol_Lihat_Keranjang_pada_notifikasi_produk_berhasil_ditambahkan_ke_keranjang() throws Throwable {
        detailProdukPage.clickLihatKeranjangButton();
    }

    @Then("^produk \"([^\"]*)\" tampil di halaman keranjang dengan alamat pengiriman \"([^\"]*)\"$")
    public void produk_tampil_di_halaman_keranjang_dengan_alamat_pengiriman(String prodName, String alamat) throws Throwable {
        keranjangPage.validateProductinKeranjang(prodName);
        keranjangPage.validateAlamatTujuan(alamat);
    }

}
