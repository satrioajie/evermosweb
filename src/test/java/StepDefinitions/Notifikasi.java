package StepDefinitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Notifikasi extends BaseStep {

    @When("^user mengklik icon notifikasi di halaman katalog$")
    public void user_mengklik_icon_notifikasi_di_halaman_katalog() throws Throwable {
        catalogPage.klikNotifikasiIcon();
    }

    @When("^user mengklik tab Evermos di halaman notifikasi$")
    public void user_mengklik_tab_Evermos_di_halaman_notifikasi() throws Throwable {
        notofikasiPage.clickEvermosTab();
    }

    @When("^user mengklik notifikasi \"([^\"]*)\" di halaman notifikasi$")
    public void user_mengklik_notifikasi_di_halaman_notifikasi(String title) throws Throwable {
        notofikasiPage.clickTitleNotifikasi(title);
    }

    @Then("^halaman detail produk \"([^\"]*)\" tampil$")
    public void halaman_detail_produk_tampil(String prodName) throws Throwable {
        detailProdukPage.validateProductName(prodName);
    }
    
}
