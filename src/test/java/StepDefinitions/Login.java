package StepDefinitions;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Login extends BaseStep{

    @Given("^user membuka halaman web evermoss$")
    public void user_membuka_halaman_web_evermoss() throws Throwable {
        halUtamaEvermos.validateHalUtamaEvermos();
    }

    @When("^user mengklik tombol Masuk di halaman utama evermos$")
    public void user_mengklik_tombol_Masuk_di_halaman_utama_evermos() throws Throwable {
        halUtamaEvermos.clickMasukButton();
    }

    @When("^user mengisi nomor telepon \"([^\"]*)\" di halaman login$")
    public void user_mengisi_nomor_telepon_di_halaman_login(String noTelp) throws Throwable {
       loginPage.fillNoTelpText(noTelp);
    }

    @When("^user mengisi password \"([^\"]*)\" di halaman login$")
    public void user_mengisi_password_di_halaman_login(String pass) throws Throwable {
       loginPage.fillPassText(pass);
    }

    @When("^user mengklik tombol Masuk di halaman login$")
    public void user_mengklik_tombol_Masuk_di_halaman_login() throws Throwable {
       loginPage.klikMasukButton();
    }

    @Then("^halaman catalog akan muncul$")
    public void halaman_catalog_akan_muncul() throws Throwable {
       catalogPage.validateTitleCatalogPage();
    }

    @Then("^muncul notifikasi error \"([^\"]*)\" di halaman login$")
    public void muncul_notifikasi_error(String errorNotif) throws Throwable {
        loginPage.validateErrorNotifExist(errorNotif);
    }

}
