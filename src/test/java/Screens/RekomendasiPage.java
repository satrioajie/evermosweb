package Screens;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;

public class RekomendasiPage extends BaseScreen {
    @FindBy(xpath = "//div[@class='selectedBrand__name']")
    WebElement brandName;

    public RekomendasiPage(WebDriver driver) {
        super(driver);
    }

    public void validateBrandName(String bN){
        Assert.assertEquals("Nama brand salah",bN,brandName.getText());
    }

    public void clickProduct(String prodName) throws InterruptedException {
        Boolean find = false;
        WebElement element;
        JavascriptExecutor jse = (JavascriptExecutor)driver;

        while(!find){
            try {
                element = driver.findElement(By.xpath("//a[.='" + prodName + "']"));
                find = true;
            }catch (Exception e){
                element = null;
                find = false;
                jse.executeScript("window.scrollBy(0,300)");
            }
        }

        element = driver.findElement(By.xpath("//a[.='" + prodName + "']"));
        jse.executeScript("arguments[0].click();", element);

    }

}
