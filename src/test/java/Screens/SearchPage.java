package Screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchPage extends BaseScreen {

    @FindBy(xpath = "//input[@class='appHeading__search__input']")
    WebElement searchBarText;

    @FindBy(xpath = "//a[.='BATAL']")
    WebElement batalButton;

    public SearchPage(WebDriver driver) {
        super(driver);
    }

    public void fillSearchBarText(String keyword){
        searchBarText.sendKeys(keyword);
    }

    public void clickSearchResult(String keyword){
        driver.findElement(By.xpath("//mark[.='"+keyword+"']")).click();
    }

    public void clickBatalButton(){
        batalButton.click();
    }
}
