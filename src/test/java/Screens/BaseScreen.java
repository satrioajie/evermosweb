package Screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class BaseScreen {
    WebDriver driver;

    BaseScreen(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }
}
