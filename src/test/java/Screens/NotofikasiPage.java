package Screens;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NotofikasiPage extends BaseScreen {
    @FindBy(xpath="//span[.='Evermos']")
    WebElement evermostab;

    public NotofikasiPage(WebDriver driver) {
        super(driver);
    }

    public void clickEvermosTab(){
        evermostab.click();
    }

    public void clickTitleNotifikasi(String title){
        WebElement element = driver.findElement(By.xpath("//div[.='"+title+"']"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        driver.findElement(By.xpath("//div[.='"+title+"']/../..")).click();
    }

}
