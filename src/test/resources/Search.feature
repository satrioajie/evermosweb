Feature: Serach Bar

Background: User login dengan credential yang valid
  Given user membuka halaman web evermoss
  And user mengklik tombol Masuk di halaman utama evermos
  And user mengisi nomor telepon "12233344445555" di halaman login
  And user mengisi password "password" di halaman login
  And user mengklik tombol Masuk di halaman login
  And halaman catalog akan muncul


Scenario: user melakukan pencarian brand dengan menggunakan search bar
  When user mengklik search bar di halaman katalog
  And user mengisi keyword "MQ Apparel" di halaman pencarian
  And user mengklik hasil pencarian "MQ Apparel" di halaman pencarian
  Then halaman brand "MQ Apparel" muncul di halaman rekomendasi


Scenario: user melakukan pembatalan pencarian
  When user mengklik search bar di halaman katalog
  And user mengisi keyword "MQ Apparel" di halaman pencarian
  And user mengklik tombol BATAL di halaman pencarian
  Then halaman catalog akan muncul