Feature: Notifikasi

Background: user login
  Given user membuka halaman web evermoss
  And user mengklik tombol Masuk di halaman utama evermos
  And user mengisi nomor telepon "12233344445555" di halaman login
  And user mengisi password "password" di halaman login
  And user mengklik tombol Masuk di halaman login
  And halaman catalog akan muncul

Scenario: Menampilkan detail notifikasi
  When user mengklik icon notifikasi di halaman katalog
  And user mengklik tab Evermos di halaman notifikasi
  And user mengklik notifikasi "Yuk Mulai Kenali Tauhid Pada Anak Dengan Produk Ini !" di halaman notifikasi
  Then halaman detail produk "Yaumi Kids Buku Buku Tauhid Pertamaku" tampil